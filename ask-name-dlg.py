# -*- coding: utf-8; -*-
import sys
from PyQt5.QtWidgets import QApplication, QInputDialog
from faker import Faker

if __name__ == '__main__':
    app = QApplication(sys.argv)
    s = Faker('ko_KR').name()
    while True:
        s, okPressed = QInputDialog.getText(
            None, "Get username", "Username:", text=s)
        if okPressed and len(s) > 0:
            print(s)
            app.quit()
            break
