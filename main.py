#!/usr/bin/env python
# -*- coding: utf-8; -*-
import sys

import chat_pb2
import chat_pb2_grpc
import grpc
from faker import Faker
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import QSize, QThread, pyqtSignal
from PyQt5.QtWidgets import (QGridLayout, QHBoxLayout, QInputDialog, QLabel,
                             QLineEdit, QMainWindow, QPushButton, QTextEdit,
                             QWidget)


class ChatStreamReceivingThread(QThread):
    incomingMessage = pyqtSignal(str, str, name='incomingMessage')

    def __init__(self, username, chatStub):
        QThread.__init__(self)
        self.username = username
        self.chatStub = chatStub

    def __del__(self):
        self.wait()

    def run(self):
        req = chat_pb2.LoginRequest(name=self.username)
        it = self.chatStub.RecieveMessages(req)
        for i in it:
            self.incomingMessage.emit(i.name, i.message)


def input_username(qapplication):
    s = Faker('ko_KR').name()
    while True:
        s, okPressed = QInputDialog.getText(
            None, "Get username", "Username:", text=s)
        if okPressed and len(s) > 0:
            return s


class ChatWindow(QMainWindow):
    sendChatMessage = pyqtSignal(str, str, name='sendChatMessage')

    def __init__(self, username, server_addr):
        self.username = username
        #
        QMainWindow.__init__(self)
        #
        self.setMinimumSize(QSize(640, 480))
        self.setWindowTitle("gRPC Chat")
        #
        centralWidget = QWidget(self)
        self.setCentralWidget(centralWidget)
        #
        gridLayout = QGridLayout(self)
        centralWidget.setLayout(gridLayout)
        # status: "Server"
        labelServer = QLabel("Server:", self)
        textServer = QLineEdit(self)
        textServer.setReadOnly(True)
        textServer.setText(server_addr)
        gridLayout.addWidget(labelServer, 0, 0)
        gridLayout.addWidget(textServer, 0, 1)
        gridLayout.setRowStretch(0, 0)
        # status: "Username"
        labelUsername = QLabel("Username:", self)
        textUsername = QLineEdit(self)
        textUsername.setReadOnly(True)
        textUsername.setText(username)
        gridLayout.addWidget(labelUsername, 1, 0)
        gridLayout.addWidget(textUsername, 1, 1)
        gridLayout.setRowStretch(1, 0)
        # Chat Log
        self.textChatLog = QTextEdit(self)
        self.textChatLog.setReadOnly(True)
        gridLayout.addWidget(self.textChatLog, 2, 0, 1, 2)
        # Send Entry and Button
        hboxSend = QHBoxLayout()
        self.textSend = QLineEdit(self)
        self.textSend.returnPressed.connect(self.onSendChatMessage)
        hboxSend.addWidget(self.textSend)
        buttonSend = QPushButton("Send", self)
        buttonSend.clicked.connect(self.onSendChatMessage)
        hboxSend.addWidget(buttonSend)
        gridLayout.addLayout(hboxSend, 3, 0, 1, 2)
        # Focus
        self.textSend.setFocus()

    def onSendChatMessage(self):
        s = self.textSend.text()
        self.textSend.clear()
        self.textSend.setFocus()
        #
        self.sendChatMessage.emit(self.username, s)

    def addChatMessage(self, username, message):
        if username == self.username:
            self.textChatLog.append(f'<i>{username}</i>: {message}')
        else:
            self.textChatLog.append(f'<b>{username}</b>: {message}')


class ChatMessageSender(object):
    def __init__(self, chatStub):
        self.chatStub = chatStub

    def __call__(self, username, s):
        req = chat_pb2.SendMessageRequest(name=username, message=s)
        reply = self.chatStub.SendMessage(req)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    # Server
    server_url = 'localhost:10000'
    # Username?
    username = input_username(app)
    #
    channel = grpc.insecure_channel(server_url)
    chatStub = chat_pb2_grpc.ChatStub(channel)
    # login
    chatStub.Login(chat_pb2.LoginRequest(name=username))
    # Main Window
    mainWin = ChatWindow(username, server_url)
    # sender
    mainWin.sendChatMessage.connect(ChatMessageSender(chatStub))
    # receiver
    recvThr = ChatStreamReceivingThread(username, chatStub)
    recvThr.incomingMessage.connect(mainWin.addChatMessage)
    recvThr.start()
    #
    mainWin.show()
    sys.exit(app.exec_())

# EOF
